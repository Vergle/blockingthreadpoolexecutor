package be.glever.threadpooltests;

import java.util.Random;
import java.util.concurrent.Callable;

public class MyCallable implements Callable<String> {
	private Random rnd = new Random();

	@Override
	public String call() throws Exception {
		int sleepyTime = rnd.nextInt(1000);
		System.out.println(Thread.currentThread() + " sleeping for " + sleepyTime);
		Thread.sleep(sleepyTime);
		System.out.println(Thread.currentThread() + " wakey wakey" );
		
		return "yes yes, I'm up";
	}
	
	

}
