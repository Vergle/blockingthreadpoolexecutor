package be.glever.threadpooltests;

import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ThreadPoolTests_Main {

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		ThreadPoolExecutor tpe = new ThreadPoolExecutor(5, 5, 0L, TimeUnit.SECONDS, new BlockOnOfferArrayBlockingQueue<Runnable>(10));
		
		ArrayList<Future<String>> results = new ArrayList<>();
		for (int i = 0; i < 100 ; i++){
			System.out.println("adding work nr " + i + " to the threadpool");
			MyCallable callable = new MyCallable();
			results.add(tpe.submit(callable));
		}
		
		for(Future<String> fut : results){
			System.out.println(fut.get());
		}
		
		tpe.shutdown();
		tpe.awaitTermination(10000, TimeUnit.MINUTES);
	}

}
