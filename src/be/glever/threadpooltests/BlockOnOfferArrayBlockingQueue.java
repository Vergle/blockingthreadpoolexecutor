package be.glever.threadpooltests;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

public class BlockOnOfferArrayBlockingQueue<V> extends ArrayBlockingQueue<V> {
	private static final long serialVersionUID = 1L;

	public BlockOnOfferArrayBlockingQueue(int capacity) {
		super(capacity);
		
		
	}

	@Override
	public boolean offer(V e, long timeout, TimeUnit unit)
			throws InterruptedException {
		super.put(e);
		return true;
	}

	@Override
	public boolean offer(V e) {
		try {
			super.put(e);
		} catch (InterruptedException e1) {
			throw new RuntimeException(e1.getMessage());
		}
		return true;
	}

	
	
}
